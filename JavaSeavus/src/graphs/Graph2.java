package graphs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class Graph2 {
  public Map<Integer, Node> nodesLookup = new HashMap<>();

  public void addNode(Node node) {
    nodesLookup.put(node.key, node);
  }

  public Node getNode(int key) {
    return nodesLookup.get(key);
  }
  
  public void removeNode(int key) {
    Node nodeForDeletion = nodesLookup.remove(key);
    for (Node node : nodesLookup.values()) {
      node.adjasentNodes.remove(nodeForDeletion);
    }
  }
  
  // Add link in both directions
  public void addEdgeBothDirections(int sourceKey, int destinationKey) {
    Node srcNode = nodesLookup.get(sourceKey);
    Node dstNode = nodesLookup.get(destinationKey);
    srcNode.adjasentNodes.add(dstNode);
    dstNode.adjasentNodes.add(srcNode);
  }
  
  public void addEdgeFromTo(int sourceKey, int destinationKey) {
    Node srcNode = nodesLookup.get(sourceKey);
    Node dstNode = nodesLookup.get(destinationKey);
    srcNode.adjasentNodes.add(dstNode);
  }


  public boolean hasPathDFS(int sourceKey, int destinationKey) {
    Node srcNode = nodesLookup.get(sourceKey);
    Node dstNode = nodesLookup.get(destinationKey);
    Set<Integer> visitedNodes = new HashSet<>();
    return hasPathDFS(srcNode, dstNode, visitedNodes);
  }
  
  private boolean hasPathDFS(Node srcNode, Node dstNode, Set<Integer> visitedNodes) {
    if (visitedNodes.contains(srcNode.key)) {
      return false;
    } else {
      visitedNodes.add(srcNode.key);
      if (srcNode == dstNode) {
        return true;
      }
      for (Node  adjasentNode : srcNode.adjasentNodes) {
        if (hasPathDFS(adjasentNode, dstNode, visitedNodes)) {
          return true;
        }
      }
    }
    return false;
  }

  public boolean hasPathBFS(int sourceKey, int destinationKey) {
    Node srcNode = nodesLookup.get(sourceKey);
    Node dstNode = nodesLookup.get(destinationKey);
    Set<Integer> visitedNodes = new HashSet<>();
    return hasPathBFS(srcNode, dstNode, visitedNodes);
  }
  
  private boolean hasPathBFS(Node srcNode, Node dstNode, Set<Integer> visitedNodes) {
    LinkedList<Node> nextToVisit = new LinkedList<>();
    nextToVisit.add(srcNode);
    while (!nextToVisit.isEmpty()) {
      Node checkNode = nextToVisit.remove();
      if (checkNode == dstNode) {
        return true;
      }
      if (visitedNodes.contains(checkNode.key)) {
        continue;
      }
      visitedNodes.add(checkNode.key);
      for (Node adjasentNode : checkNode.adjasentNodes) {
        nextToVisit.add(adjasentNode);
      }
    }
    return false;
  }

  public static class Node {

    int key;
    String value;
    LinkedList<Node> adjasentNodes = new LinkedList<>();

    public Node(int key, String value) {
      this.key = key;
      this.value = value;
    }

  }
  
  public static class Link {
    
    final Node adjasentNode;
    final int weight;
    
    public Link(Node adjasentNode, int weight) {
      this.adjasentNode = adjasentNode;
      this.weight = weight;
    }
    
  }
  
  public static void main(String[] args) {
    Graph2 graph = new Graph2();
    graph.addNode(new Node(1, "Skopje"));
    graph.addNode(new Node(2, "Bitola"));
    graph.addNode(new Node(3, "Ohrid"));
    graph.addNode(new Node(4, "Solun"));
    graph.addNode(new Node(5, "Denver"));
    System.out.println(graph.hasPathBFS(1, 4));
    System.out.println(graph.hasPathDFS(1, 4));

    graph.addEdgeBothDirections(1, 2);
    graph.addEdgeBothDirections(1, 3);
    graph.addEdgeBothDirections(3, 4);
    System.out.println(graph.hasPathBFS(1, 4));
    System.out.println(graph.hasPathDFS(1, 4));
    
    System.out.println(graph.hasPathBFS(1, 5));
    System.out.println(graph.hasPathDFS(1, 5));
    
//    graph.removeNode(3);
//    System.out.println(graph.hasPathBFS(1, 4));
    
    System.out.println(graph.hasPathBFS(4, 1));
    
  }

}