package graphs;
// DFS - Depth First Search - to each node in depth - recursive but tricky (use isVisitedFlag)
// BFS - Breadth First Search - search level by level (wide before deep)

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class Graph {
  private Map<Integer, Node> nodeLookup = new HashMap<>();
  
  public static class Node {
    private int id;
    LinkedList<Node> adjacent = new LinkedList<>();
    private Node(int id) {
      this.id = id;
    }
  }
  
  public Node getNode(int id) {
    return nodeLookup.get(id);
  }
  
  public void addNode(int id, Node node) {
    nodeLookup.put(id, node);
  }
  
  public void addEdge(int source, int destination) {
    Node s = getNode(source);
    Node d = getNode(destination);
    s.adjacent.add(d);
  }
  
  public boolean hasPathDFS(int source, int destination) {
    Node s = getNode(source);
    Node d = getNode(destination);
    Set<Integer> visited = new HashSet<>(); // Better hare than in the node (helps with immutability)
    return hasPathDFS(s, d, visited);
  }

  private boolean hasPathDFS(Node source, Node destination, Set<Integer> visited) {
    if (visited.contains(source.id)) {
      return false;
    } else {
      visited.add(source.id);
      if (source == destination) {
        return true;
      }
      for (Node child : source.adjacent) {
        if (hasPathDFS(child, destination, visited)) {
          return true;
        }
      }
    }
    return false;
  }
  
  public boolean hashPathBFS(int source, int destination) {
    return hashPathBFS(getNode(source), getNode(destination));
  }
  /**
   * Best for finding of the shortest path
   * @param source
   * @param destination
   * @return
   */
  private boolean hashPathBFS(Node source, Node destination) {
    LinkedList<Node> nextToVisit = new LinkedList<>();
    Set<Integer> visited = new HashSet<>();
    nextToVisit.add(source);
    while (!nextToVisit.isEmpty()) {
      Node node = nextToVisit.remove();
      if (node == destination) {
        return true;
      }
      if (visited.contains(node.id)) {
        continue;
      }
      visited.add(node.id);
      for (Node child : node.adjacent) {
        nextToVisit.add(child);
      }
    }
    return false;
  }
  
}