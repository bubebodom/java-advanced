
public class TestFibonachi {

  public static void main(String[] args) {
    System.out.println(fibonachi(5));
  }

  private static int fibonachi(int n) {
    int fibonachi = 0;
    if (n == 0) { // terminal condition
      fibonachi = 0;
    } else if (1 <= n && n <= 2 ) { // terminal condition
      fibonachi = 1;
    } else {
      fibonachi = fibonachi(n - 1) + fibonachi(n - 2);
    }
    return fibonachi;
  }

}
