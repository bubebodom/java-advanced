import java.util.HashMap;
import java.util.Map;

public class TestFibonachiMemorization {

  private static Map<Integer, Integer> calculatedFibonachies = new HashMap<>();
  
  public static void main(String[] args) {
    System.out.println(fibonachi(5));
  }

  private static int fibonachi(int n) {
    int fibonachi = 0;
    if (n == 0) { // terminal condition
      fibonachi = 0;
    } else if (1 <= n && n <= 2 ) { // terminal condition
      fibonachi = 1;
    } else {
      //check if fibonachi(n - 1) i fibonachi(n - 2) exist in hashmap calculatedFibonachies. if they don't exist invoke the function
      fibonachi = fibonachi(n - 1) + fibonachi(n - 2);
    }
      // put calculated value in hashmap
    calculatedFibonachies.put(n, fibonachi);
    return fibonachi;
  }
}
