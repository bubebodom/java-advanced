
public class TestFactoriel {

  public static void main(String[] args) {
    System.out.println(factoriel(5));
  }

  private static int factoriel(int n) {
    int factoriel = 0;
    if (n <= 1) { // terminal condition
      factoriel = 1;
    } else {
      factoriel = n * factoriel(n - 1);
    }
    return factoriel;
  }

}
