package hashtable.lectures;

public class TestSimpleMapOne {

  public static void main(String[] args) {
    SimpleMapOne map = new SimpleMapOne(20);
    
    map.insert("-1");
    map.insert("20");
    map.insert("0");
    map.insert("19");
    map.insert("11");
    System.out.println(map.toString());
    System.out.println();
    
    System.out.println(map.doesExist("-1"));
    System.out.println(map.doesExist("20"));
    System.out.println(map.doesExist("0"));
    System.out.println(map.doesExist("19"));
    System.out.println(map.doesExist("11"));
    System.out.println(map.toString());
    System.out.println();
    
    System.out.println(map.getIndex("-1"));
    System.out.println(map.getIndex("20"));
    System.out.println(map.getIndex("0"));
    System.out.println(map.getIndex("19"));
    System.out.println(map.getIndex("11"));
    System.out.println(map.toString());
    System.out.println();

  }

}
