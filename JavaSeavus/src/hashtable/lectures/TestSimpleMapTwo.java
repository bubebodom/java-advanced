package hashtable.lectures;

public class TestSimpleMapTwo {

  public static void main(String[] args) {
    SimpleMapTwo map = new SimpleMapTwo(20, 1, 9999);
    
    map.insert("0");
    map.insert("10000");
    map.insert("1");
    map.insert("9999");
    map.insert("556");
    System.out.println(map.toString());
    System.out.println();
    map.insert("576");
    System.out.println(map.toString());
    System.out.println();
    
    System.out.println(map.doesExist("0"));
    System.out.println(map.doesExist("10000"));
    System.out.println(map.doesExist("1"));
    System.out.println(map.doesExist("9999"));
    System.out.println(map.doesExist("556"));
    System.out.println(map.toString());
    System.out.println();
    
    System.out.println(map.getIndex("0"));
    System.out.println(map.getIndex("10000"));
    System.out.println(map.getIndex("1"));
    System.out.println(map.getIndex("9999"));
    System.out.println(map.getIndex("556"));
    System.out.println(map.getIndex("576"));
    System.out.println(map.toString());
    System.out.println();

  }

}
