package hashtable.lectures;

import java.util.Arrays;

public class SimpleMapOne {
  private String[] array;
  private int size;
  
  public SimpleMapOne(int size) {
    this.size = size;
    this.array = new String[size];
    Arrays.fill(array, "-1");
  }
  
  public SimpleMapOne insert(String value) {
    int intValue = Integer.parseInt(value);
    if (0 <= intValue && intValue < size) {
      array[intValue] = value;
    } else {
      System.out.println("not a propriate value: " + value);
    }
    return this;
  }
  
  public boolean doesExist(String value) {
    int intValue = Integer.parseInt(value);
    if (0 <= intValue && intValue < size && array[intValue].equals(value)) {
      return true;
    } else {
      return false;
    }
  }
  
  public int getIndex(String value) {
    int intValue = Integer.parseInt(value);
    if (0 <= intValue && intValue < size && array[intValue].equals(value)) {
      return intValue;
    } else {
      return -1;
    }
  }
  
  @Override
  public String toString() {
    return Arrays.toString(array);
  }
  
}