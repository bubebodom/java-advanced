package hashtable.lectures;

import java.util.Arrays;

public class SimpleMapTwo {
  private String[] array;
  private int size, minValue, maxValue;
  
  public SimpleMapTwo(int size, int minValue, int maxValue) {
    this.size = size;
    this.minValue = minValue;
    this.maxValue = maxValue;
    this.array = new String[size];
    Arrays.fill(array, "-1");
  }
  
  public int getHash(String value) {
    int hash = -1;
    int intValue = Integer.parseInt(value);
    if (minValue <= intValue && intValue <= maxValue) {
      hash = intValue % size;
    }
    return hash;
  }
  
  public SimpleMapTwo insert(String value) {
    int hash = getHash(value);
    if (-1 < hash) {
      array[hash] = value;
    } else {
      System.out.println("not a propriate value: " + value);
    }
    return this;
  }
  
  public boolean doesExist(String value) {
    int hash = getHash(value);
    if (0 <= hash && array[hash].equals(value)) {
      return true;
    } else {
      return false;
    }
  }
  
  public int getIndex(String value) {
    int hash = getHash(value);
    if (0 <= hash && array[hash].equals(value)) {
      return hash;
    } else {
      return -1;
    }
  }
  
  @Override
  public String toString() {
    return Arrays.toString(array);
  }
  
}