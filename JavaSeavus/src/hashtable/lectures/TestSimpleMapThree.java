package hashtable.lectures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestSimpleMapThree {

  public static void main(String[] args) {
    
    List<String>[] myArray = new ArrayList[10];
    Arrays.fill(myArray, new ArrayList<String>());
    
    SimpleMapThree map = new SimpleMapThree(20, 1, 9999);
    map.insert("0");
    map.insert("10000");
    map.insert("1");
    map.insert("9999");
    map.insert("556");
    System.out.println(map.toString());
    System.out.println();
    map.insert("576");
    System.out.println(map.toString());
    System.out.println();
    map.insert("596");
    System.out.println(map.toString());
    System.out.println();
    map.insert("636");
    map.insert("636");
    map.insert("636");
    map.insert("636");
    System.out.println(map.toString());
    System.out.println();
    map.insert("676");
    System.out.println(map.toString());
    System.out.println();
    
    System.out.println(map.doesExist("0"));
    System.out.println(map.doesExist("10000"));
    System.out.println(map.doesExist("1"));
    System.out.println(map.doesExist("9999"));
    System.out.println(map.doesExist("556"));
    System.out.println(map.doesExist("576"));
    System.out.println(map.toString());
    System.out.println();
    
    System.out.println(map.getIndex("0"));
    System.out.println(map.getIndex("10000"));
    System.out.println(map.getIndex("1"));
    System.out.println(map.getIndex("9999"));
    System.out.println(map.getIndex("556"));
    System.out.println(map.getIndex("576"));
    System.out.println(map.getIndex("596"));
    System.out.println(map.getIndex("636"));
    System.out.println(map.toString());
    System.out.println();
    
    map.delete("636");
    System.out.println(map.toString());
    System.out.println();
  }

}
