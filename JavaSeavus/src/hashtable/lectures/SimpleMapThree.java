package hashtable.lectures;

import java.util.Arrays;

public class SimpleMapThree {
  private String[] array;
  private int size, arraySize, minValue, maxValue, counter;
  
  public SimpleMapThree(int size, int minValue, int maxValue) {
    this.size = size;
    this.minValue = minValue;
    this.maxValue = maxValue;
    this.arraySize = size * 2;
    this.array = new String[arraySize];
    Arrays.fill(array, "-1");
  }
  
  public int getHash(String value) {
    int hash = -1;
    int intValue = Integer.parseInt(value);
    if (minValue <= intValue && intValue <= maxValue) {
      hash = intValue % arraySize;
    }
    return hash;
  }
  
  public SimpleMapThree insert(String value) {
    int hash = getHash(value);
    if (-1 < hash && counter < size) {
      if (array[hash].equals("-1")) {
        array[hash] = value;
        counter++;
      } else {
        int i = hash + 1;
        for (; i < array.length; i++) {
          if (array[i].equals("-1")) {
            break;
          }
        }
        if (i == array.length) {
          i = 0;
          for (; i < hash; i++) {
            if (array[i].equals("-1")) {
              break;
            }
          }
        }
        if (array[i].equals("-1")) {
          array[i] = value;
          counter++;
        } else {
          System.out.println("ova ne smee da se sluci!");
        }
      }
    } else {
      System.out.println("counter exceeded or not a propriate value: " + value);
    }
    return this;
  }
  
  public boolean doesExist(String value) {
    int hash = getHash(value);
    if (-1 < hash) {
      if (array[hash].equals(value)) {
        return true;
      } else {
        int i = hash + 1;
        for (; i < array.length; i++) {
          if (array[i].equals(value)) {
            return true;
          }
        }
        if (i == array.length) {
          i = 0;
          for (; i < hash; i++) {
            if (array[i].equals(value)) {
              return true;
            }
          }
        }
      }
    } else {
      System.out.println("Not a propriate value: " + value);
    }
    return false;
  }
  
  public int getIndex(String value) {
    int hash = getHash(value);
    if (-1 < hash) {
      if (array[hash].equals(value)) {
        return hash;
      } else {
        int i = hash + 1;
        for (; i < array.length; i++) {
          if (array[i].equals(value)) {
            return i;
          }
        }
        if (i == array.length) {
          i = 0;
          for (; i < hash; i++) {
            if (array[i].equals(value)) {
              return i;
            }
          }
        }
      }
    } else {
      System.out.println("Not a propriate value: " + value);
    }
    return -1;
  }
  
  public void delete(String value) {
    for (int i = 0; i < array.length; i++) {
      if (array[i].equals(value)) {
        array[i] = "-1";
        counter--;
      }
    }
  }
  
  @Override
  public String toString() {
    return Arrays.toString(array);
  }
  
}