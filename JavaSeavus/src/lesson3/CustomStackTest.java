package lesson3;

import java.util.Arrays;

public class CustomStackTest {

  public static void main(String[] args) {

    CustomStack stack = new CustomStack();
    stack.push(2);
    stack.push(7);
    stack.push(5);
    stack.push(4);
    stack.push(1);
    stack.printStack();
    stack.push(3);
    stack.printStack();
    Object o = stack.pop();
    stack.printStack();
    System.out.println("Element is " + o);

  }

}

class CustomStack {
  private static final int INITIAL_SIZE = 5;
  private Object[] elements;
  private int currentElementIndex = 0;

  public CustomStack() {
    elements = new Object[INITIAL_SIZE];
  }

  public Object pop() {
    Object elementToPop = elements[--currentElementIndex];
    elements[currentElementIndex] = null;
    if (currentElementIndex + INITIAL_SIZE <= elements.length) {
      compressStack();
    }
    return elementToPop;
  }

  private void compressStack() {
    elements = Arrays.copyOf(elements, elements.length / 2);
  }

  public void push(Object elementToPush) {
    if (currentElementIndex == elements.length) {
      expandStack();
    }
    elements[currentElementIndex++] = elementToPush;
  }

  public void printStack() {
    System.out.println(Arrays.toString(elements));
  }

  public int getSize() {
    return elements.length;
  }

  private void expandStack() {
    elements = Arrays.copyOf(elements, elements.length * 2);
  }
}