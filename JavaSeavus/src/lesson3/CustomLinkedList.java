package lesson3;

public class CustomLinkedList {

    private Node headNode;
    private int nodeIndex;


    public static void main(String[] args) {
        CustomLinkedList linkedList = new CustomLinkedList(10);
        linkedList.printList();
        linkedList.addFirst("Hello");
        linkedList.addLast("bube");
        linkedList.addFirst(1);
        linkedList.addLast("java");
        linkedList.printList();
        linkedList.addAtIndex("index", 2);
        linkedList.printList();
        linkedList.deleteFirst();
        linkedList.printList();
        linkedList.deleteLast();
        linkedList.printList();
        linkedList.deleteAtIndex(0);
        linkedList.printList();
    }

    public CustomLinkedList(Object value) {
        headNode = new Node(value);
    }

    public void addFirst(Object value) {
        Node newHeadNode = new Node(value);
        newHeadNode.setNext(headNode);
        headNode = newHeadNode;
        nodeIndex++;
    }

    public void addLast(Object value) {
        Node temp = headNode;
        for (int i = 0; i < nodeIndex; i++) {
            temp = temp.getNext();
        }
        temp.setNext(new Node(value));
        nodeIndex++;
    }

    public void addAtIndex(Object value, int index) {
        Node temp = headNode;
        if (index == 0)
            return;
        for (int i = 0; i < index - 1 && temp.getNext() != null; i++) {
            temp = temp.getNext();
        }
        Node holder = temp.getNext();
        Node newNode = new Node(value);
        newNode.setNext(holder);
        temp.setNext(newNode);
        nodeIndex++;
    }

    public void deleteFirst() {
        Node temp = headNode;
        if (temp.getNext() != null) {
            headNode = temp.getNext();
        }
        nodeIndex--;
    }

    public void deleteLast() {
        Node temp = headNode;
        for (int i = 0; i < nodeIndex; i++) {
            temp = temp.getNext();
        }
        temp.setNext(null);
        nodeIndex--;
    }

    public void deleteAtIndex(int index) {
        Node temp = headNode;
        if (index == 0)
            return;
        for (int i = 0; i < index - 1 && temp.getNext() != null; i++) {
            temp = temp.getNext();
        }
        Node holder = temp.getNext().getNext();
        temp.setNext(holder);
        nodeIndex--;
    }

    public void printList() {
        Node temp = headNode;
        System.out.print("[ ");
        for (int i = 0; i <= nodeIndex; i++) {
            System.out.print(temp.getValue());
            if (i != nodeIndex)
                System.out.print(", ");
            if (temp.getNext() != null)
                temp = temp.getNext();
        }
        System.out.println(" ]");
    }

}

class Node {
    private Object value;
    private Node next;

    public Node(Object value) {
        this.value = value;

    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Object getValue() {
        return value;
    }
}
