package lesson3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class ArrayListAndIterator {

    public static void main(String[] args) {

        String[] colors = {"RED", "YELLOW", "BLUE", "PINK", "GREEN"};

        List<String> colorsList = new ArrayList<>();
        for (String color : colors) {
            colorsList.add(color);
        }
        System.out.println("Colors list: " + colorsList);

        List<String> colorsForRemoval = new ArrayList<>();
        colorsForRemoval.add("YELLOW");
        colorsForRemoval.add("BLUE");
        colorsForRemoval.add("PINK");

        removeColors(colorsList, colorsForRemoval);

        System.out.println("Colors list after removal: " + colorsList);

    }

    private static void removeColors(Collection<String> colorsList, Collection<String> colorsForRemoval) {
        Iterator<String> iterator = colorsList.iterator();
        while (iterator.hasNext()) {
            if (colorsForRemoval.contains(iterator.next())) {
                iterator.remove();
            }
        }
    }

}
