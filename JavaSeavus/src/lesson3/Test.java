package lesson3;

import java.util.*;

public class Test {

    public static void main(String[] args) {

        String[] colorsArray = {"white", "red", "yellow", "blue", "green"};
        List<String> colorsList = new ArrayList<>();
        for (int i = 0; i < colorsArray.length; i++) {
            colorsList.add(colorsArray[i]);
        }

        System.out.println("List of colors: " + colorsList);

        String[] colorsForRemovalArray = {"red", "blue"};
        List<String> colorsForRemovalList = Arrays.asList(colorsForRemovalArray);

        System.out.println("Colors for removal: " + colorsForRemovalList);

        removeColors(colorsList, colorsForRemovalList);

        System.out.println("Colors are removed: " + colorsList);
    }

    /**
     * This method removes elements from first list which are contained in the second list
     * @param colorsList
     * @param colorsForRemovalList
     */
    private static void removeColors(List<String> colorsList, List<String> colorsForRemovalList) {
        Iterator iterator = colorsList.iterator();
        while (iterator.hasNext()) {
            if (colorsForRemovalList.contains(iterator.next())) {
                iterator.remove();
            }
        }
    }

}
