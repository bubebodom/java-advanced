package lesson3;

import static javafx.scene.input.KeyCode.T;

public class QueueTest {

  public static void main(String[] args) {

  }

}

class Queue {
  private NodeQ headNode;
  private int nodeIndex;

  public Queue(Object value) {
    this.headNode = new NodeQ(value);
  }

  public void addFirst(Object value) {
    NodeQ newHeadNode = new NodeQ(value);
    newHeadNode.setNext(headNode);
    headNode = newHeadNode;
    nodeIndex++;
  }

  public void addLast(Object value) {
    NodeQ temp = headNode;
    for (int i = 0; i < nodeIndex; i++) {
      temp = temp.getNext();
    }
    temp.setNext(new NodeQ(value));
    nodeIndex++;
  }

  public void printList() {
    NodeQ temp = headNode;
    System.out.print("[ ");
    for (int i = 0; i <= nodeIndex; i++) {
      System.out.print(temp.getValue());
      if (i != nodeIndex)
        System.out.print(", ");
      if (temp.getNext() != null)
        temp = temp.getNext();
    }
    System.out.println(" ]");
  }
}

class NodeQ {
  private Object value;
  private NodeQ next;

  public NodeQ(Object value) {
    this.value = value;
  }

  public Object getValue() {
    return value;
  }

  public NodeQ getNext() {
    return next;
  }

  public void setNext(NodeQ next) {
    this.next = next;
  }
}
