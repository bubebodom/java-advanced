package lesson3;

import java.util.LinkedList;

public class JavaLinkedListTest {

    public static void main(String[] args) {
        CusLinkedList linkedList = new CusLinkedList(123);
        linkedList.addLast("Hello");
        linkedList.addLast(55);
        linkedList.printList();
        LinkedList<String> l = new LinkedList<>();

    }

}

class CusLinkedList {
    private CussNode head;
    private int maxIndex;

    public CusLinkedList(Object value) {
        this.head = new CussNode(value);
    }

    public void addLast(Object value) {
        CussNode temp = head;
        for (int i = 0; i < maxIndex; i++) {
            temp = temp.getNext();
        }
        temp.setNext(new CussNode(value));
        maxIndex++;
    }

    public void addFirst(Object value) {

    }

    public void printList() {
        CussNode temp = head;
        System.out.print("[ ");
        for (int i = 0; i <= maxIndex; i++) {
            System.out.print(temp.getValue());
            if (i != maxIndex)
                System.out.print(", ");
            if (temp.getNext() != null)
                temp = temp.getNext();
        }
        System.out.println(" ]");
    }
}

class CussNode {
    private CussNode next;
    private Object value;

    public CussNode(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public CussNode getNext() {
        return next;
    }

    public void setNext(CussNode next) {
        this.next = next;
    }
}
