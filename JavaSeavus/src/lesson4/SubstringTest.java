package lesson4;

public class SubstringTest {

    public static void main(String[] args) {
        String s = "Java Course";
        System.out.println(s.substring(1, 4));
        System.out.println(s.lastIndexOf("a"));
    }

}
