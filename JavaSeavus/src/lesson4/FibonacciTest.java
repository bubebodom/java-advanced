package lesson4;

public class FibonacciTest {

  public static void main(String[] args) {

    System.out.print("Fibonacci numbers: ");
    for (int i = 1; i <= 20; i++) {
      System.out.print(fibonacci(i) + " ");
    }

  }

  private static int fibonacci(int n) {
    if (n == 0)
      return 0;
    if (n == 1)
      return 1;
    return fibonacci(n - 1) + fibonacci(n - 2);
  }

}
