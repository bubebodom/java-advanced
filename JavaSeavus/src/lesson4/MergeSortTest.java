
package lesson4;

import java.util.Arrays;

public class MergeSortTest {
  private static int[] randomArray = {65, 23, 83, 42, 12, 3, 98, 45, 34, 23, 24, 77};

  public static void main(String[] args) {
    System.out.println("Unsorted array: " + Arrays.toString(randomArray));
    sort();
    System.out.println("Sorted array: " + Arrays.toString(randomArray));
  }

  private static void sort() {
    sortArray(0, randomArray.length - 1);
  }

  private static void sortArray(int low, int high) {
    if ((high - low) >= 1) {
      int middle1 = (low + high) / 2;
      int middle2 = middle1 + 1;
      sortArray(low, middle1);
      sortArray(middle2, high);
      merge(low, middle1, middle2, high);
    }
  }

  private static void merge(int low, int middle1, int middle2, int high) {
    int lowIndex = low;
    int middle2Index = middle2;
    int combinedIndex = low;
    int[] combined = new int[randomArray.length];
    
    while (lowIndex <= middle1 && middle2Index <= high) {
      if (randomArray[lowIndex] <= randomArray[middle2Index]) {
        combined[combinedIndex++] = randomArray[lowIndex++];
      } else {
        combined[combinedIndex++] = randomArray[middle2Index++];
      }
    }
    
    if (lowIndex == middle2) {
      while (middle2Index <= high) {
        combined[combinedIndex++] = randomArray[middle2Index++];
      }
    } else {
      while (lowIndex <= middle1) {
        combined[combinedIndex++] = randomArray[lowIndex++];
      }
    }
    
    for (int i = low; i <= high; i++) {
      randomArray[i] = combined[i];
    }
    
  }

}