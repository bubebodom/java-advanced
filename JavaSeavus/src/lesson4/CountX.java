package recursion;

public class CountX {

  public static void main(String[] args) {
    System.out.println(strCount("xyx", "x"));
    System.out.println(countX("XxLxxLAKjsdli kn"));
  }
  
  private static int countX(String str) {
    if (str.lastIndexOf("x") == -1)
      return 0;
    return 1 + countX(str.substring(0, str.lastIndexOf("x")));
  }
  
  private static int strCount(String str, String sub) {
    if (str.lastIndexOf(sub) == -1)
      return 0;
    return 1 + strCount(str.substring(0, str.lastIndexOf(sub)), sub);
  }
  
}
