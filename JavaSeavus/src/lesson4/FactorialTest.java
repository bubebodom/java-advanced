package lesson4;

public class FactorialTest {

  public static void main(String[] args) {

    for (int i = 0; i <= 10; i++) {
      System.out.println(i + "! = " + factorial(i));
    }

  }

  private static long factorial(long n) {
    if (n < 1)
      throw new NumberFormatException("Can't calculate factorial of values smaller than 1!");
    if (n == 1)
      return 1;
    return n * factorial(n - 1);
  }

}
