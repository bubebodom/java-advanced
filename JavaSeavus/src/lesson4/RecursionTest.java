package lesson4;

public class RecursionTest {

    public static void main(String[] args) {
        int i = 4;
        System.out.println(i + "! = " + factorial(i));
    }

    private static int factorial(int n) {
        if (n == 1)
            return 1;
        return n * factorial(n - 1);
    }

}
