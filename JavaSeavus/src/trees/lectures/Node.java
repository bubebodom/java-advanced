package trees.lectures;

public class Node {

  public boolean isRoot = false;
  
  public Node leftChild;
  public Node rightChild;
  
  public int key;
  public String value;
 
  
  
  public Node(int key, String value) {
    this.key = key;
    this.value = value;
  }



  public boolean add(Node leafNode) {
    boolean isSuccess = false;
    if (leafNode.key == key) {
      isSuccess = false;
    } else if (leafNode.key > key) {
      if (rightChild == null) {
        rightChild = leafNode;
        isSuccess = true;
      } else {
        isSuccess = rightChild.add(leafNode);
      }
    } else if (leafNode.key < key) {
      if (leftChild == null) {
        leftChild = leafNode;
        isSuccess = true;
      } else {
        isSuccess = leftChild.add(leafNode);
      }
    }
    return isSuccess;
  }



  public String findByKey(int searchKey) {
    String value = null;
    if (this.key == searchKey) {
      value = this.value;
    } else if (searchKey > this.key) {
      if (rightChild == null) {
        value = null;
      } else {
        value = rightChild.findByKey(searchKey);
      }
    } else if (searchKey < this.key) {
      if (leftChild == null) {
        value = null;
      } else {
        value = leftChild.findByKey(searchKey);
      }
    }
    return value;
  }



  @Override
  public String toString() {
    return "Node [isRoot=" + isRoot + ", key=" + key + ", value=" + value + "]";
  }
    
}