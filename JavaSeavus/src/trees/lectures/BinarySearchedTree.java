package trees.lectures;

import java.util.ArrayList;
import java.util.List;

public class BinarySearchedTree {

  public Node rootNode;

  public BinarySearchedTree() {
  }

  public boolean add(int key, String value) {
    boolean isSuccess = false;
    if (rootNode == null) {
      rootNode = new Node(key, value);
      rootNode.isRoot = true;
      isSuccess = true;
    } else {
      rootNode.add(new Node(key, value));
    }
    return isSuccess;
  }

  public String findByKey(int searchKey) {
    String value = null;
    if (rootNode == null) {
      value = null;
    } else {
      value = rootNode.findByKey(searchKey);
    }
    return value;
  }

  public List<Node> inOrderTraversal() { // left, root, right
    List<Node> nodes = new ArrayList<>();
    if (rootNode == null) {
      nodes = new ArrayList<>();
    } else {
      nodes = inOrderTraversal(rootNode);
    }
    return nodes;
  }
  
  private List<Node> inOrderTraversal(Node node) {
    List<Node> nodes = new ArrayList<>();
    if (node != null) {
      nodes.addAll(inOrderTraversal(node.leftChild));
      nodes.add(node);
      nodes.addAll(inOrderTraversal(node.rightChild));
    }
    return nodes;
  }

  public List<Node> preOrderTraversal() { // root, left, right
    List<Node> nodes = new ArrayList<>();
    if (rootNode == null) {
      nodes = new ArrayList<>();
    } else {
      nodes = preOrderTraversal(rootNode);
    }
    return nodes;
  }
  
  private List<Node> preOrderTraversal(Node node) {
    List<Node> nodes = new ArrayList<>();
    if (node != null) {
      nodes.add(node);
      nodes.addAll(preOrderTraversal(node.leftChild));
      nodes.addAll(preOrderTraversal(node.rightChild));
    }
    return nodes;
  }

  public List<Node> postOrderTraversal() { // left, right, root
    List<Node> nodes = new ArrayList<>();
    if (rootNode == null) {
      nodes = new ArrayList<>();
    } else {
      nodes = postOrderTraversal(rootNode);
    }
    return nodes;
  }
  
  private List<Node> postOrderTraversal(Node node) {
    List<Node> nodes = new ArrayList<>();
    if (node != null) {
      nodes.addAll(postOrderTraversal(node.leftChild));
      nodes.addAll(postOrderTraversal(node.rightChild));
      nodes.add(node);
    }
    return nodes;
  }
  
    public int getKeyForValue(String value) {
      int key = -1;
      List<Node> nodes = inOrderTraversal();
      for (Node node : nodes) {
        if (node.value == value) {
          key = node.key;
          break;
        }
      }
      return key;
    }
}