package trees.lectures;

import java.util.List;

public class TestBST {

  public static void main(String[] args) {

    BinarySearchedTree bst = new BinarySearchedTree();
    bst.add(35, "igor");
    bst.add(23, "maja");
    bst.add(34, "pero");
    bst.add(56, "");
    System.out.println(bst.findByKey(35));
    System.out.println(bst.findByKey(56));
    System.out.println(bst.findByKey(34));
    System.out.println(bst.findByKey(23));
    List<Node> nodes = bst.inOrderTraversal();
    for (Node node : nodes) {
      System.out.println(node);
    }
    
    System.out.println();
    System.out.println(bst.getKeyForValue("pero"));
    System.out.println(bst.getKeyForValue("jasna"));
  }

}