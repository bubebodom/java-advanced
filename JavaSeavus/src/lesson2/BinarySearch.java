package lesson2;

import java.util.Arrays;

public class BinarySearch {
    public static void main(String[] args) {
        System.out.println();
        BinaryArray binaryArray = new BinaryArray();
        int location = binaryArray.binarySearch(46);
        if (location != -1)
            System.out.println("Key found in location " + location + "!");
        else
            System.out.println("Key not found!");

    }
}

class BinaryArray {
    int[] binaryArray = {11, 12, 17, 19, 21, 23, 28, 29, 34, 39, 46, 55, 61, 77, 81, 88, 97};

    public int binarySearch(int searchKey) {
        int low = 0;
        int high = binaryArray.length - 1;
        int middle = (low + high + 1) / 2;
        int location = -1;

        do {
            System.out.print(remaining(low, high));
            for ( int i = 0; i < middle; i++ )
                System.out.print( "   " );
            System.out.println( " * " );

            if (searchKey == binaryArray[middle])
                location = middle;
            else if (searchKey > binaryArray[middle])
                low = middle + 1;
            else
                high = middle - 1;
            middle = (low + high + 1) / 2;
        } while ((low <= high) && (location == -1));

        return location;
    }

    private String remaining(int low, int high) {

        StringBuilder temporary = new StringBuilder();

        for (int i = 0; i < low; i++) {
            temporary.append("   ");
        }

        for (int i = low; i <= high; i++) {
            temporary.append(binaryArray[i] + " ");
        }

        temporary.append("\n");
        return temporary.toString();
    }

    @Override
    public String toString() {
        return Arrays.toString(binaryArray);
    }
}