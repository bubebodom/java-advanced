package lesson2;

import java.util.Arrays;

public class InsertionSort {

    public static void main(String[] args) {
        InsertionArray array = new InsertionArray();
        System.out.println("Unsorted array \n" + array.toString());
        array.sort();
        System.out.println("Sorted array \n" + array.toString());
    }

}

class InsertionArray {
    int[] array = {36, 76, 32, 67, 98, 12, 5, 2, 48, 55};

    public void sort() {
        int insert;
        int moveIndex;
        for (int next = 1; next < array.length; next++) {
            insert = array[next];
            moveIndex = next;
            while (moveIndex > 0 && array[moveIndex - 1] > insert) {
                array[moveIndex] = array[moveIndex - 1];
                moveIndex--;
            }
            array[moveIndex] = insert;
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }
}
