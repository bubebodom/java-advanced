package lesson2;

import java.util.Arrays;

public class SelectionSort {
    public static void main(String[] args) {

        SelectionArray array = new SelectionArray();
        System.out.println("Unsorted array \n" + array.toString());
        array.sort();
        System.out.println("Sorted array \n" + array.toString());


    }
}

class SelectionArray {
    int[] array = {36, 76, 32, 67, 98, 12, 5, 2, 48, 55};

    public void sort() {
        int smallest;

        for (int i = 0; i < array.length - 1; i++) {
            smallest = i;
            for (int index = i; index < array.length; index++) {
                if (array[index] < array[smallest])
                    smallest = index;
            }
            swap(i, smallest);
        }
    }

    private void swap(int first, int second) {
        int temporary = array[first];
        array[first] = array[second];
        array[second] = temporary;
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }
}