package lesson2;

import java.util.Arrays;
import java.util.Random;

public class LinearSearch {

    public static void main(String[] args) {
        LinearArray linearArray = new LinearArray(10);
        System.out.println(linearArray.toString());
        int searchKey = 11;
        int index = linearArray.linearSearch(searchKey);

        if (index != -1)
            System.out.println("The number " + searchKey + " was found on index " + index);
        else
            System.out.println("The number " + searchKey + " was not found!");
    }
}

class LinearArray {

    int[] linearArray;
    Random generator = new Random();

    public LinearArray(int arraySize) {
        linearArray = new int[arraySize];
        for (int i = 0; i < arraySize; i++)
            linearArray[i] = generator.nextInt(20);
    }

    public int linearSearch(int searchKey) {
        for (int index = 0; index < linearArray.length; index++) {
            if (linearArray[index] == searchKey)
                return index;
        }        
        return -1;
    }


    @Override
    public String toString() {
        return Arrays.toString(linearArray) + "\n";
    }
}
