package lesson2;

public class Test {

    public static void main(String[] args) {
        String[] iminja = {"Darko", "Tanja", "Zlatko", "Jane", "Zoki"};
        String searchKey = "Tanja";

        for (int i = 0; i < iminja.length; i ++) {
            if (searchKey.equals(iminja[i])) {
                System.out.println("Index is " + i);
            }
        }
    }
}
