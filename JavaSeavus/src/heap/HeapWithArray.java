package heap;

import java.util.Arrays;

//1. Looks like a binary tree where each parent is smaller than its children
//2. It can be easely represented as an array since nodes are added sequentially
//3. for each node with index idx its parent has index (idx - 1)/2; and children are 
//for left idx * 2  + 1 and for right idx * 2  + 2
//4. it can be with the higher indexes on-top or the smaller indexes on-top
//5. It can contain duplicates
//6. Add - element is added as the lest element in the array. Than the largest child 
//its parent is moved upwards (generally not needed to check the both children only the inserted one)
//7. Remove - remove the first item. put the last one in its place. swap positions with the smallest / largest child recursively 
public class HeapWithArray {

  public Node[] array;
  private int lastIndex = -1;

  public HeapWithArray(int size) {
    this.array = new Node[size];
  }

  private void resizeTheArray() {
    Node[] newArray = new Node[array.length * 2];
    for (int i = 0; i < array.length; i++) {
      newArray[i] = array[i];
    }
    this.array = newArray;
  }

  public boolean push(Node node) {
    if (lastIndex == array.length -1) {
      resizeTheArray();
    }
    lastIndex++;
    array[lastIndex] = node;
    performHeapUp(lastIndex);
    return true;
  }

  public Node peek() {
    Node node = null;
    if (lastIndex >= 0) {
      node = array[0];
    }
    return node;
  }

  private void performHeapUp(int childIndex) {
    if (childIndex == 0) {
      return;
    }
    //parentIndex = (idx - 1)/2
    int parentIndex = (childIndex - 1)/2;
    if (array[childIndex].key < array[parentIndex].key) {
      Node tmpNode = array[parentIndex];
      array[parentIndex] = array[childIndex];
      array[childIndex] = tmpNode;
      performHeapUp(parentIndex);
    }

  }

  public Node pop() {
    Node node = null;
    if (lastIndex >= 0) {
      node = array[0];
      array[0] = array[lastIndex];
      array[lastIndex] = null;
      lastIndex--;
      performHeapDown(0);
    }
    return node;
  }

  private void performHeapDown(int parentIndex) {
    //left idx * 2  + 1 and for right idx * 2  + 2
    if (lastIndex < 0 || array[parentIndex * 2  + 1] == null) {
      return;
    }
    int leftIndex = parentIndex * 2  + 1;
    int rightIndex = parentIndex * 2  + 2;
    Node left = array[leftIndex];
    Node right = array[rightIndex];
    int minIndex;
    if (right == null || left.key <= right.key) {
      minIndex = leftIndex;
    } else {
      minIndex = rightIndex;
    }
    if (array[parentIndex].key > array[minIndex].key) {
      Node tmpNode = array[parentIndex];
      array[parentIndex] = array[minIndex];
      array[minIndex] = tmpNode;
      performHeapDown(minIndex);
    }
  }

  @Override
  public String toString() {
    return Arrays.toString(array);
  }

}

class Node {
  int key;
  String Value;
  Node(int key, String value) {
    this.key = key;
    this.Value = value;
  }

  @Override
  public String toString() {
    return "Node [key=" + key + ", Value=" + Value + "]";
  }
}