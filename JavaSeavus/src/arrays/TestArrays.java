package arrays;

import java.util.Arrays;

public class TestArrays {

  public static void main(String[] args) {
    Integer [] integs = new Integer[] {1, 2, 3, 4, 5, 6};
    System.out.println(Arrays.toString(integs));
    
    Integer[] newIntArray = Arrays.copyOf(integs, integs.length);
    System.out.println(Arrays.toString(newIntArray));
    
    Integer[] newIntArray2 = Arrays.copyOfRange(integs, 1, 3);
    System.out.println(Arrays.toString(newIntArray2));
    
    Arrays.fill(integs, new Integer(1));
    System.out.println(Arrays.toString(integs));
    
    
    String[] names = new String[] {"igor", "janko", "Stanko"};
    System.out.println(Arrays.toString(names));
    System.out.println(Arrays.binarySearch(names, "Janko"));
    System.out.println(Arrays.binarySearch(names, "Janko", new MyComparator()));
    
    int[][] ints = new int[3][3];
    ints[0][0] = 1;
    for (int[] is : ints) {
      Arrays.fill(is, 1);
    }
    System.out.println(Arrays.toString(ints));
    for (int[] is : ints) {
      System.out.println(Arrays.toString(is));
    }
    int[][][] ints3 = new int[3][4][5];
    
    //homework
//    char[] - dali se cita isto od levo na desno i od desno na levo
//           - sortiraj go arejto
//           
//    matrica - pecatenje na matrica - probajte so rekurzija
//            - ogledalno preslikuvanje na matrica 4x4
    
  }

  
}