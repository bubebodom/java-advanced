package stackandqueue.stack;
//Arrays, linked lists, trees, etc. are best for data that represents real objects (groups of real objects).

public class TheStackStarter {

  private String[] stackArray;
  private int stackSize;

  // Sets stack as empty
  private int topOfStackIndex = -1;

  public void displayTheStack(){
    for(int n = 0; n < 61; n++) {
      System.out.print("-");
    }
    System.out.println();
    for (int n = 0; n < stackSize; n++) {
      System.out.format("| %2s "+ " ", n);
    }
    System.out.println("|");
    for (int n = 0; n < 61; n++) {
      System.out.print("-");
    }
    System.out.println();
    for (int n = 0; n < stackSize; n++) {
      if(stackArray[n].equals("-1")) {
        System.out.print("|     ");
      } else {
        System.out.print(String.format("| %2s "+ " ", stackArray[n]));
      }
    }
    System.out.println("|");
    for (int n = 0; n < 61; n++) {
      System.out.print("-");
    }
    System.out.println();

  }

}