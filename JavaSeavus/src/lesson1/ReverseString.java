package lesson1;

public class ReverseString {

    public static void main(String[] args) {
        System.out.println(reverseString("Java interview"));
    }

    public static String reverseString(String s) {
        String reverseString = "";
        for (int i = s.length() - 1; i >= 0; i--) {
            reverseString = reverseString + s.charAt(i);
        }
        return reverseString;
    }

}
