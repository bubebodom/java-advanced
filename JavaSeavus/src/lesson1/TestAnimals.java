package lesson1;

import java.util.ArrayList;

public class TestAnimals {

    public static void main(String[] args) {
        ArrayList<Animal> animals = new ArrayList<>();
        Kangaroo kangaroo = new Kangaroo("kevin", "treva", 2);
        Wolf wolf = new Wolf("Jovan", "zajcinja", 4);
        Cat cat = new Cat("Maca", "riba", 4);

        animals.add(kangaroo);
        animals.add(wolf);
        animals.add(cat);

        for (Animal zivotno : animals) {
            System.out.println("Ime na zivotno? " + zivotno.getName());
            System.out.println("Omilena hrana na zivotno? " + zivotno.getFavoriteFood());
            System.out.println("Broj na noze? " + zivotno.getNumberOfLegs());
        }

    }

}
