package stackandqueue.queue;

import java.util.Arrays;

public class TheCircularQueue {

  private int head; 
  private int tail;
  
  private final int capacity;
  private final String[] queueArray;
  
  public TheCircularQueue(int capacity) {
    this.head = 0;
    this.tail = 0;
    this.capacity = capacity;
    this.queueArray = new String[capacity];
    Arrays.fill(queueArray, "-1");
  }
  
  public boolean enQueue(String element) {
    boolean success;
    if (!isFull()) {
      if (isEmpty()) {
        queueArray[tail] = element;
      } else {
        tail = (tail + 1) % capacity;
        queueArray[tail] = element;
      }
      success = true;
    } else {
      success = false;
    }
    return success;
  }
  
  public String deQueue() {
    String element;
    if (!isEmpty()) {
      element = queueArray[head];
      queueArray[head] = "-1";
      if (!isEmpty()) {
        head = (head + 1) % capacity;
      }
    } else {
      element = null;
    }
    return element;
  }
  
  public boolean isEmpty() {
    boolean empty;
    if ((head == tail)
        && queueArray[head].equals("-1")) {
      empty = true;
    } else {
      empty = false;
    }
    return empty;
  }
  
  public boolean isFull() {
    boolean full;
    if (head == ((tail + 1) % capacity)) {
      full = true;
    } else {
      full = false;
    }
    return full;
  }
  
}