package stackandqueue.queue;

public class TheQueueStarter {

  private String[] queueArray;
  private int queueSize;

  // Sets stack as empty
  private int front, numberOfItems, rear = 0;

  public void displayTheQueue() {
    for(int n = 0; n < 61; n++) {
      System.out.print("-");
    }
    System.out.println();
    for (int n = 0; n < queueSize; n++) {
      System.out.format("| %2s "+ " ", n);
    }
    System.out.println("|");
    for (int n = 0; n < 61; n++) {
      System.out.print("-");
    }
    System.out.println();
    for (int n = 0; n < queueSize; n++) {
      if (queueArray[n].equals("-1")) {
        System.out.print("|     ");
      } else {
        System.out.print(String.format("| %2s "+ " ", queueArray[n]));
      }
    }
    System.out.println("|");
    for (int n = 0; n < 61; n++) {
      System.out.print("-");
    }
    System.out.println();
    // Number of spaces to put before the F
    int spacesBeforeFront = 3*(2*(front+1)-1);
    for (int k = 1; k < spacesBeforeFront; k++) {
      System.out.print(" ");
    }
    System.out.print("F");
    // Number of spaces to put before the R
    int spacesBeforeRear = (2*(3*rear)-1) - (spacesBeforeFront);
    for (int l = 0; l < spacesBeforeRear; l++) {
      System.out.print(" ");
    }
    System.out.print("R");
    System.out.println("\n");
  }

}