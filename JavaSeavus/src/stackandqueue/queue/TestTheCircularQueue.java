package stackandqueue.queue;

public class TestTheCircularQueue {

  public static void main(String[] args) {
    TheCircularQueue queue = new TheCircularQueue(10);
    queue.enQueue("baba");
    queue.enQueue("dedo");
    System.out.println(queue.deQueue());
    System.out.println(queue.deQueue());
    System.out.println(queue.deQueue());
    System.out.println(queue.deQueue());
    
    queue.enQueue("baba");
    queue.enQueue("dedo");
    System.out.println(queue.deQueue());
    System.out.println(queue.deQueue());
    System.out.println(queue.deQueue());
    System.out.println(queue.deQueue());
  }
  
}
