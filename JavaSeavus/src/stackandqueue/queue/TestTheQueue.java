package stackandqueue.queue;

public class TestTheQueue {
  public static void main(String[] args){

    TheQueue theQueue = new TheQueue(10);

    theQueue.enQueue("16");
    theQueue.displayTheQueue();
    theQueue.deQueue();
    theQueue.displayTheQueue();

//    theQueue.priorityEnQueue("25");

    theQueue.priorityEnQueue("10");

    /*
    theQueue.insert("10");

    theQueue.displayTheQueue();

    theQueue.insert("15");

    theQueue.displayTheQueue();

    theQueue.insert("25");

    theQueue.displayTheQueue();

    theQueue.insert("25");

    theQueue.displayTheQueue();

    theQueue.insert("25");
     */

    theQueue.displayTheQueue();

    theQueue.deQueue();

    theQueue.displayTheQueue();

    theQueue.deQueue();

    theQueue.displayTheQueue();

    theQueue.peek();

  }
  
}