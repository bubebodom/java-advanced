package stackandqueue.stack;
//Arrays, linked lists, trees, etc. are best for data that represents real objects (groups of real objects).

//Stacks & Queues are instead used to complete a task and are soon after discarded.

//Stacks & Queues
//1. Allow only a single item to be added or removed at a time
//2. Stacks allow access to the last item inserted (LIFO)
//3. Queues allow access to the first item inserted (FIFO)

import java.util.Arrays;

public class TheStack {

  private String[] stackArray;
  private int stackSize;

  // Sets stack as empty
  private int topOfStackIndex = -1;

  public TheStack(int size) {
    stackSize = size;
    stackArray = new String[size];
    // Assigns the value of -1 to every value in the array so to control what gets printed to screen
    Arrays.fill(stackArray, "-1");
  }

  public void push(String input) {
    if (topOfStackIndex + 1 < stackSize) {
      topOfStackIndex++;
      stackArray[topOfStackIndex] = input;
    } else {
      System.out.println("Sorry But the Stack is Full");
    }
    displayTheStack();
    System.out.println("PUSH " + input + " was added to the stack\n");
  }

  public String pop() {
    if (topOfStackIndex >= 0) {
      displayTheStack();
      System.out.println("POP " + stackArray[topOfStackIndex] + " Was Removed From the Stack\n");
      // Just like in memory an item isn't deleted, but instead is just not available
      stackArray[topOfStackIndex] = "-1"; // Assigns -1 so the value won't appear
      return stackArray[topOfStackIndex--]; // first the evaluation is performed, than the decrement is performed
    } else {
      displayTheStack();
      System.out.println("Sorry But the Stack is Empty");
      return "-1";
    }


  }

  public String peek() {
    displayTheStack();
    System.out.println("PEEK " + stackArray[topOfStackIndex] + " Is at the Top of the Stack\n");
    return stackArray[topOfStackIndex];
  }

  public void pushMany(String multipleValues) {
    String[] tempString = multipleValues.split(" ");
    for (String string : tempString) {
      push(string);
    }
  }

  public String popAll() {
    String result = "";
    for(int i = topOfStackIndex; i >= 0; i--){
      result = pop() + " " + result;
    }
    return result;
  }

  public void popDisplayAll(){
    System.out.println("The result: " + popAll());
  }

  public void displayTheStack(){
    for(int n = 0; n < 61; n++) {
      System.out.print("-");
    }
    System.out.println();
    for (int n = 0; n < stackSize; n++) {
      System.out.format("| %2s "+ " ", n);
    }
    System.out.println("|");
    for (int n = 0; n < 61; n++) {
      System.out.print("-");
    }
    System.out.println();
    for (int n = 0; n < stackSize; n++) {
      if(stackArray[n].equals("-1")) {
        System.out.print("|     ");
      } else {
        System.out.print(String.format("| %2s "+ " ", stackArray[n]));
      }
    }
    System.out.println("|");
    for (int n = 0; n < 61; n++) {
      System.out.print("-");
    }
    System.out.println();

  }

}