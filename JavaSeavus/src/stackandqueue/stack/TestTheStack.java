package stackandqueue.stack;

public class TestTheStack {

  public static void main(String[] args){

    TheStack theStack = new TheStack(10);

    theStack.push("10");
    theStack.push("17");
    theStack.push("13");

    // Look at the top value on the stack
    theStack.peek();

    // Remove values from the stack (LIFO)
    theStack.pop();
    theStack.pop();
    theStack.pop();

    // Add many to the stack
    theStack.pushMany("T O R N A DO");

    // Remove all from the stack
    // theStack.popAll();

    // Remove all from the stack and print them

    theStack.popDisplayAll();

    theStack.displayTheStack();


  }

}