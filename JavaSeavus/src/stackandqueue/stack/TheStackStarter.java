package stackandqueue.stack;

import java.util.Arrays;

//Arrays, linked lists, trees, etc. are best for data that represents real objects (groups of real objects).

public class TheStackStarter {

  private String[] stackArray;
  private int capacity;

  // Sets stack as empty
  private int topOfStackIndex = -1;
  
  public TheStackStarter(int cap) {
    stackArray = new String[cap];
    Arrays.fill(stackArray, "-1");
    this.capacity = cap;
  }

  public boolean push(String value) {
    boolean pushed = false;
    if (topOfStackIndex < capacity -1) {
      topOfStackIndex = topOfStackIndex + 1;
      stackArray[topOfStackIndex] = value; 
      pushed = true;
    }
    return pushed;
  }
  
  public String pop() {
    String value = null;
    if (topOfStackIndex > -1) {
      value = stackArray[topOfStackIndex];
      stackArray[topOfStackIndex] = "-1";
      topOfStackIndex--;
    }
    return value;
  }
  
  public String peak() {
    String value = null;
    if (topOfStackIndex > -1) {
      value = stackArray[topOfStackIndex];
    }
    return value;
  }
  
  public void displayTheStack(){
    for(int n = 0; n < 61; n++) {
      System.out.print("-");
    }
    System.out.println();
    for (int n = 0; n < capacity; n++) {
      System.out.format("| %2s "+ " ", n);
    }
    System.out.println("|");
    for (int n = 0; n < 61; n++) {
      System.out.print("-");
    }
    System.out.println();
    for (int n = 0; n < capacity; n++) {
      if(stackArray[n].equals("-1")) {
        System.out.print("|     ");
      } else {
        System.out.print(String.format("| %2s "+ " ", stackArray[n]));
      }
    }
    System.out.println("|");
    for (int n = 0; n < 61; n++) {
      System.out.print("-");
    }
    System.out.println();

  }

}