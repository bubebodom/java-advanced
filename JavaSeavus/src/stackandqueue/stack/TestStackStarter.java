package stackandqueue.stack;

public class TestStackStarter {

  public static void main(String[] args) {
    TheStackStarter stack = new TheStackStarter(5);
    stack.push("1");
    stack.displayTheStack();
    stack.push("15");
    stack.displayTheStack();
    System.out.println(stack.pop());
    stack.displayTheStack();
    stack.push("11");
    stack.displayTheStack();
    System.out.println(stack.peak());
    stack.displayTheStack();

  }

}
