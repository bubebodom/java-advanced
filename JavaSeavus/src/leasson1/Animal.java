package leasson1;

public abstract class Animal {
    private String name;
    private String favoriteFood;
    private int numberOfLegs;

    public Animal (String name, String favoriteFood, int numberOfLegs) {
        this.name = name;
        this.favoriteFood = favoriteFood;
        this.numberOfLegs = numberOfLegs;
    }

    public String getName() {
        return name;
    }

    public String getFavoriteFood() {
        return favoriteFood;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFavoriteFood(String favoriteFood) {
        this.favoriteFood = favoriteFood;
    }

    public void setNumberOfLegs(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }
}
