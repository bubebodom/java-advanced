package leasson1;

public class Cat extends Animal {

    public Cat(String name, String favoriteFood, int numberOfLegs) {
        super(name, favoriteFood, numberOfLegs);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getFavoriteFood() {
        return super.getFavoriteFood();
    }

    @Override
    public int getNumberOfLegs() {
        return super.getNumberOfLegs();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public void setFavoriteFood(String favoriteFood) {
        super.setFavoriteFood(favoriteFood);
    }

    @Override
    public void setNumberOfLegs(int numberOfLegs) {
        super.setNumberOfLegs(numberOfLegs);
    }
}
