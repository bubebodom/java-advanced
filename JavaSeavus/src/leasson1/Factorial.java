package leasson1;

public class Factorial {

    public static void main(String[] args) {
        System.out.println(factorial(4));
    }

    public static int factorial(int n) {
        for (int i = n - 1; i > 1; i--) {
            n *= i;
        }
        return n;
    }

}
